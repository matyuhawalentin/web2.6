<!DOCTYPE html>

<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <meta charset="utf-8">
  <title>WEB6</title>
  <link rel="stylesheet" href="style.css">
  <style>
    .error {
      border: 2px solid red;
    }

    .error-radio {
      border: 2px solid red;
    }

    .error-bio {
      border: 2px solid #ff0000;
      width: 250px;
      height: 16px;
    }

    .error-check {
      border: 2px solid red;
      width: 350px;
    }

    .error-abil {
      border: 2px solid red;
      width: 200px;
      height: 70px;
    }

    .error-radio-limb {
      border: 2px solid red;
      width: 250px;
      height: 35px;
    }

    .messageLogin {
      max-width: max-content;
      margin: 0 auto;
      margin-top: 300px;
    }
  </style>
</head>

<body>
  <?php
  if (!empty($messages)) {
    print('<div id="messages" class="messageLogin">');
    // Выводим все сообщения.
    foreach ($messages as $message) {
      print($message);
    }
    print('</div>');
  }

  // Далее выводим форму отмечая элементы с ошибками классом error
  // и задавая начальные значения элементов ранее сохраненными.
  ?>
  <button onclick="document.location='login.php'">Авторизация</button><br />
  <button onclick="document.location='admin.php'">Админ</button>
  <div class="container border rounded-5 shadow bodyContainer">
    <div class="nameForm m-auto">Форма</div>
    <form action="index.php" id="my-formcarry" accept-charset="UTF-8" method="POST">
      <div class="flex-column">
        <div class="c">
          ФИО:

          <input id="formname" type="text" name="fio" placeholder="Введите имя" <?php if ($errors['fio']) {
                                                                                  print 'class="error"';
                                                                                } ?> value="<?php print $values['fio']; ?>">
        </div>
        <div class="c">
          E-mail:
          <input id="formmail" type="email" name="email" placeholder="Введите почту" <?php if ($errors['email']) {
                                                                                        print 'class="error"';
                                                                                      } ?> value="<?php print $values['email']; ?>">
        </div>
        <div class="c">
          <p>Ваш год рождения:</p>
          <input <?php if ($errors['year']) {
                    print 'class="error"';
                  } ?> value="<?php print $values['year_value']; ?>" id="dr" name="birthyear" value="" type="date" placeholder="Год рождения" />
        </div>
        <div class="c">
          <p>Пол:</p>
          <label <?php if ($errors['sex']) {
                    print 'class="error-radio"';
                  } ?>>
            <input <?php if ($values['sex_value'] == "man") {
                      print 'checked';
                    } ?> type="radio" name="radio2" value="man" />
            Мужской</label>
          <label <?php if ($errors['sex']) {
                    print 'class="error-radio"';
                  } ?>> <input <?php if ($values['sex_value'] == "woman") {
                                                                                    print 'checked';
                                                                                  } ?> type="radio" name="radio2" value="woman" />
            Женский</label>
          <p>Количество конечностей</p>
          <div <?php if ($errors['limb']) {
                  print 'class="error-radio-limb"';
                } ?>>
            <label><input <?php if ($values['limb_value'] == "1") {
                            print 'checked';
                          } ?> type="radio" name="radio1" value="1" />
              1</label>
            <label><input <?php if ($values['limb_value'] == "2") {
                            print 'checked';
                          } ?> type="radio" name="radio1" value="2" />
              2</label>
            <label><input <?php if ($values['limb_value'] == "3") {
                            print 'checked';
                          } ?> type="radio" name="radio1" value="3" />
              3</label>
            <label><input <?php if ($values['limb_value'] == "4") {
                            print 'checked';
                          } ?> type="radio" name="radio1" value="4" />
              4</label>
            <label><input <?php if ($values['limb_value'] == "5") {
                            print 'checked';
                          } ?> type="radio" name="radio1" value="5" />
              5</label>
          </div>
        </div>
        <div class="c">
          <p>Сверхспособности</p>

          <div <?php if ($errors['abil']) {
                  print 'class="error-abil"';
                } ?>> <select id="sp" name="superpower[]" multiple="multiple">
              <option <?php if ($values['abil_value'] == "1") {
                        print 'selected';
                      } ?> value="fly">Полет</option>
              <option <?php if ($values['abil_value'] == "1") {
                        print 'selected';
                      } ?> value="immortality">Бессмертие</option>
              <option <?php if ($values['abil_value'] == "1") {
                        print 'selected';
                      } ?> value="telepathy">Чтение мыслей</option>
              <option <?php if ($values['abil_value'] == "1") {
                        print 'selected';
                      } ?> value="telekinesis">Телекинез</option>
              <option <?php if ($values['abil_value'] == "1") {
                        print 'selected';
                      } ?> value="teleportation">Телепортация</option>
            </select> </div>
        </div>
        <div class="c">
          <p id="bio">Биография:</p>
          <label <?php if ($errors['bio']) {
                    print 'class="error-bio"';
                  } ?>>
            Напишите про себя: <br />
            <textarea id="biog" name="textarea1" placeholder="Пиши тут"><?php print $values['bio_value']; ?></textarea>
          </label>
        </div>
        <div class="c">
          <div <?php if ($errors['check']) {
                  print 'class="error-check"';
                } ?>><label><input <?php if ($values['check_value'] == "1") {
                                                                                              print 'checked';
                                                                                            } ?> id="formcheck" type="checkbox" name="checkbox" value="1">Согласие на обработку персональных данных</label></div>
        </div>
      </div>
      <div class="m-auto button p-3">
        <input type="submit" value="Отправить" id="formsend" class="btn btn-success ">
      </div>
    </form>
  </div>
</body>

</html>